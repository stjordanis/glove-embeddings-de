#!/bin/bash


### Download and preprocess wiki corpus if specified in docker-compose.yml
if [ "$DOWNLOAD_WIKI" == "true"  ]
then
    echo "Downloading Wikipedia dump: $WIKI_PREFIX$WIKI_DUMP"
    wget $WIKI_PREFIX$WIKI_DUMP
fi

if [ "$CORPUS_TYPE" == "wiki" ]
then
    echo "Preprocessing Wikipedia Dump"
    python3 process_wikipedia.py $WIKI_DUMP $CORPUS
fi

## train glove
set -e
make

BUILDDIR=build
COOCCURRENCE_FILE=$OUTPUTDIR/cooccurrence.bin
COOCCURRENCE_SHUF_FILE=$OUTPUTDIR/cooccurrence.shuf.bin
VOCAB_FILE=$OUTPUTDIR/vocab.txt
SAVE_FILE=$OUTPUTDIR/vectors

echo "$ $BUILDDIR/vocab_count -min-count $VOCAB_MIN_COUNT -verbose $VERBOSE < $CORPUS > $VOCAB_FILE"
$BUILDDIR/vocab_count -min-count $VOCAB_MIN_COUNT -verbose $VERBOSE < $CORPUS > $VOCAB_FILE
echo "$ $BUILDDIR/cooccur -memory $MEMORY -vocab-file $VOCAB_FILE -verbose $VERBOSE -window-size $WINDOW_SIZE < $CORPUS > $COOCCURRENCE_FILE"
$BUILDDIR/cooccur -memory $MEMORY -vocab-file $VOCAB_FILE -verbose $VERBOSE -window-size $WINDOW_SIZE < $CORPUS > $COOCCURRENCE_FILE
echo "$ $BUILDDIR/shuffle -memory $MEMORY -verbose $VERBOSE < $COOCCURRENCE_FILE > $COOCCURRENCE_SHUF_FILE"
$BUILDDIR/shuffle -memory $MEMORY -verbose $VERBOSE < $COOCCURRENCE_FILE > $COOCCURRENCE_SHUF_FILE
echo "$ $BUILDDIR/glove -save-file $SAVE_FILE -threads $NUM_THREADS -input-file $COOCCURRENCE_SHUF_FILE -x-max $X_MAX -iter $MAX_ITER -vector-size $VECTOR_SIZE -binary $BINARY -vocab-file $VOCAB_FILE -verbose $VERBOSE"
$BUILDDIR/glove -save-file $SAVE_FILE -threads $NUM_THREADS -input-file $COOCCURRENCE_SHUF_FILE -x-max $X_MAX -iter $MAX_ITER -vector-size $VECTOR_SIZE -binary $BINARY -vocab-file $VOCAB_FILE -verbose $VERBOSE

## write model meta file
VOCAB_SIZE=$(cat $VOCAB_FILE | wc -l)
DATE=$(date +%Y-%m-%d)
echo "{
  \"alias\": \"glove\",
  \"embeddings_filename\": \"vectors.txt\",
  \"vocab_filename\": \"vocab.txt\",
  \"input_field\": \"$TEXT_TYPE\",
  \"token_spacer\": \"$TOKEN_SPACER\",
  \"vocab_size\": $VOCAB_SIZE,
  \"train_params\": {
    \"VOCAB_MIN_COUNT\": $VOCAB_MIN_COUNT,
    \"VECTOR_SIZE\": $VECTOR_SIZE,
    \"MAX_ITER\": $MAX_ITER,
    \"WINDOW_SIZE\": $WINDOW_SIZE,
    \"X_MAX\": $X_MAX
  },
  \"train_date\": \"$DATE\",
  \"notes\": \"$META_NOTES\"
}" > $OUTPUTDIR/word_emb.meta